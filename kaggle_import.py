import kaggle
import os
from glob import iglob
from zipfile import ZipFile



api=kaggle.KaggleApi()
api.authenticate()
dataset = 'bwandowando/ukraine-russian-crisis-twitter-dataset-1-2-m-rows'
dataset_path = "data/"

print("downloading dataset...")
os.system("mkdir ./data")
api.dataset_download_files(dataset,dataset_path)
print("extracting...")

os.system("mkdir ./data/extracted")

with ZipFile("./data/ukraine-russian-crisis-twitter-dataset-1-2-m-rows.zip", "r") as zip:
    zip.extractall("./data/extracted")

print("done")

