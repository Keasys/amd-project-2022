from math import sqrt
from spark_installer import spark
import gzip
import re
from functools import reduce

from random import random, randint

from glob import iglob

import pyspark
import csv

from pyspark.sql.types import StructType,StructField, StringType, IntegerType





chars_to_trim = {",",'"',".",":",";",",","`","'","!","?"}

def mapEquals(x):
    x_base = {i: 0 for i in list(set(x))}
    for i in x:
        x_base[i]+=1
    return [(i,x_base[i]) for i in x_base.keys()] if x_base.keys() != [] else [("",0)]
    
    
def trim(word, chars):
    # use regular expression 
    return word[:-1] if word[-1] in chars else word

def getRowVector(user):
    
    pass

def getMentions(text):
    return [trim(i,chars_to_trim) for i in text.split() if i[0] == "@" and text is not None] if text is not None else ""

def getHashtags(text):
    return [trim(i,chars_to_trim) for i in text.split() if i[0] == "#" and text is not None] if text is not None else ""

def JaccardSimilarity(x,y):
    try:
        return len(set(x) & set(y)) / len(set(x) | set(y))
    except ZeroDivisionError as zd:
        return  0

def Prod(A,B):
    return A.cartesian(B).filter(lambda x: x[0][0][1] == x[1][0][0]).map(lambda x: ((x[0][0][0], x[1][0][1]),x[0][1]*x[1][1])).reduceByKey(lambda x,y: x+y)

def groupHashtagsByUser(df):
    user_hashtag=df.rdd.flatMap(lambda x:[(x["username"], i) for i in getHashtags(x["text"])])
    user_hashtags= df.rdd.map(lambda x:(x["username"], getHashtags(x["text"])))
    user_hashtags_grouped=user_hashtags.groupByKey().mapValues(list).map(lambda x: (x[0], reduce(lambda a,b: a+b, x[1])))
    user_hashtags_paired=user_hashtags_grouped.cartesian(user_hashtags_grouped).filter(lambda x: x[0][0] != x[1][0])
    return user_hashtags_grouped, user_hashtags_paired

def computeDistance(users, n, metric='jaccard'):
    distance_user_user=user_hashtags_paired.map(lambda x: (x[0][0], (list(set(x[1][1]) - set(x[0][1])), x[1][0], JaccardSimilarity(x[0][1],x[1][1]) if metric == 'jaccard' else CosineDistance(x[0][1],x[1][1])))).sortBy(lambda x: x[1][2], ascending=False if metric == 'jaccard' else True).filter(lambda x: x[1][2] < 1)
    distance_user_user=distance_user_user.map(lambda x: [(x[0], i,x[1][1], x[1][2]) for i in x[1][0]] if len(x[1][0]) > 0 else (x[0],None, x[1][1], x[1][2]))
    return distance_user_user.take(n)

def RMSE(A,B):
    square_difference_matrix = A.cartesian(B).filter(lambda x: x[0][0] == x[1][0]).map(lambda x: (x[0][0], (x[0][1] - x[1][1])**2))
    RMSE = square_difference_matrix.map(lambda x: x[1]).reduce(lambda x,y: x+y) / (hashtags.count() * users.count())
    return sqrt(RMSE)

def ImproveU(r,s,U,V):
    non_improving_rows = U.cartesian(V).filter(lambda x: x[0][0][1] == x[1][0][0] and x[0][0][1] != s and x[0][0][0] == r).map(lambda x: (x[1][0][1], (x[0][1] * x[1][1])))
    error_contribution=M.filter(lambda x: x[0][0]  == r)
    sub = non_improving_rows.cartesian(error_contribution).filter(lambda x: x[1][0][1]== x[0][0]).map(lambda x: (x[0][0],x[1][1]-x[0][1]) )
    num = sub.cartesian(V).filter(lambda x: x[0][0] == x[1][0][1] and x[1][0][0] == s).map(lambda x: x[0][1]*x[1][1]).reduce(lambda x,y:x+y)
    den = V.filter(lambda x: x[0][0] == s).map(lambda x: x[1]**2).reduce(lambda x,y: x+y)
    return num/den

def ImproveV(r,s,U,V):
    non_improving_rows = U.cartesian(V).filter(lambda x: x[0][0][1] == x[1][0][0] and x[0][0][1] != r and x[1][0][1] == s).map(lambda x: (x[0][0][0], (x[0][1] * x[1][1])))
    error_contribution=M.filter(lambda x: x[0][1]  == s)
    sub = non_improving_rows.cartesian(error_contribution).filter(lambda x: x[1][0][0]== x[0][0]).map(lambda x: (x[0][0],x[1][1]-x[0][1]) )
    try:
        num = sub.cartesian(U).filter(lambda x: x[0][0] == x[1][0][0] and x[1][0][1] == r).map(lambda x: x[0][1]*x[1][1]).reduce(lambda x,y:x+y)
    except Exception as e:
        print("[ERROR] --", e)
        
        print("-"*24)
        for i in U.cartesian(V).filter(lambda x: x[0][0][1] == x[1][0][0] and x[0][0][1] != r and x[1][0][1] == s).take(5):
            print(i)
        for i in error_contribution.collect():
            print(i)

    den = U.filter(lambda x: x[0][1] == r).map(lambda x: x[1]**2).reduce(lambda x,y: x+y)
    return num/den

def OptimizationLoop(U,V,matrix="random"):
    
    
    #choose matrix to optimize
    opt = (ImproveU if random() >= 0.5 else ImproveV) if matrix == "random" else (ImproveU if matrix == "U" else ImproveV)
    
    #choose element to optimize at random
    r,s = (randint(1,U.count()/2), randint(1,2)) if opt.__name__ == "ImproveU" else (randint(1,2), randint(1,V.count()/2))
    print(opt.__name__, r, s)
    
    # choosing optimized value for U
    estimate = opt(r,s,U,V)

    if opt.__name__ == "ImproveU":
        print(f"Improving Matrix U: ({r}, {s})")
        U = U.map(lambda x: (x[0],estimate) if x[0] == (r,s) else (x[0],x[1]))
    else:
        print(f"Improving Matrix V: ({r}, {s})")
        V = V.map(lambda x: (x[0], estimate) if x[0] == (r,s) else (x[0],x[1]))

    return U,V




sc = spark.sparkContext
applying_schema = StructType([
    
    StructField("id", IntegerType(), False),
    StructField("userid", IntegerType(), False),
    StructField("username", StringType(), False),
    StructField("account_description", StringType(), True),
    StructField("location", StringType(), True),
    StructField("following", IntegerType(), False),
    StructField("followers", IntegerType(), False),
    StructField("total_tweets", IntegerType(), True),
    StructField("user_created_ts", StringType(), False),
    StructField("tweetid", IntegerType(), False),
    StructField("tweet_created_ts", StringType(), False),
    StructField("retweet_count", IntegerType(), False),
    StructField("text", StringType(), True),
    StructField("hashtags", StringType(), True),
    StructField("language", StringType(), False),
    StructField("coordinates", StringType(), True),
    StructField("favorite_count", IntegerType(), False),
    StructField("extracted_ts", StringType(),False)              

])

loadpath="./data/extracted/0402_UkraineCombinedTweetsDeduped.csv"
df=spark.read.format("csv").options(multiline="true", header="false").schema(applying_schema).load(loadpath)
df_sampled=df.sample(1/500) [df["retweet_count"] > 1] [df["language"] == "en"]

hashtags = df_sampled.rdd.flatMap(lambda x: getHashtags(x["text"])).distinct().sortBy(lambda x: x).zipWithIndex().map(lambda x:(x[0],x[1]+1))
users = df_sampled.rdd.filter(lambda x: getHashtags(x["text"]) != []).map(lambda x: x["username"]).distinct().sortBy(lambda x: x).zipWithIndex().map(lambda x:(x[0],x[1]+1))

print("sampling data: ", df_sampled.count(), " on ", df.count())

user_hashtags_grouped, user_hashtags_paired = groupHashtagsByUser(df_sampled)


#####
hashtags_singles = user_hashtags_grouped.flatMap(lambda x: [(i,x[0]) for i in x[1]])




non_removable_hashtags=hashtags_singles.groupByKey().mapValues(len)


hashtags=hashtags.cartesian(non_removable_hashtags).filter(lambda x: x[0][0] ==x[1][0]).map(lambda x: (x[0][0], x[0][1], True if x[1][1] > 2 else False) )

users=users.cartesian(user_hashtags_grouped).filter(lambda x : x[0][0] == x[1][0]).map(lambda x: (x[0][0], x[0][1], len(x[1][1]) > 2))


# removable_hashtags = hashtags.filter(lambda x: x[2]).map(lambda x: (x[0],x[1]))
# hashtags = hashtags.map(lambda x: (x[0], x[1]))


######

examples = computeDistance(user_hashtags_paired,75)

with open('jaccard_distance.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(examples)

print()

#BUILDING UTILITY MATRIX
 
def getUtilityMatrix(usergrouped,users,hashtags, test=False):
    utility_matrix_hashtags=usergrouped.map(lambda x: (x[0],mapEquals(x[1]))).filter(lambda x: x[1] != []).sortBy(lambda x: max([i[1] for i in x[1]]), ascending = False).flatMap(lambda x: [(x[0],i) for i in x[1]])
    cartesian_prod = utility_matrix_hashtags.cartesian(hashtags).filter(lambda x:x[0][1][0] == x[1][0]).map(lambda x: (x[0][0], (x[0][1], x[1])))
    print("cartesian_prod")
    # for i in cartesian_prod.take(15):
    #    print(i)
    # k: user
    # v: # hashtags, hashtag index
    row_vector_elems = cartesian_prod.groupByKey().mapValues(list).map(lambda x: (x[0],sorted(x[1], key=lambda a: a[1][1])))

    cartesian_prod_w_users = cartesian_prod.cartesian(users).filter(lambda x: x[0][0] == x[1][0])

    # print("cartesian prod w users")
    # for i in cartesian_prod_w_users.take(15):
    #     print(i)


    
 
    for i in cartesian_prod_w_users.take(10): print(i)

    removable_elems=cartesian_prod_w_users.filter(lambda x: x[0][1][1][2] and x[1][2]).map(lambda x: ((x[1][1],x[0][1][1][1]), x[0][1][0][1]) )
    # for i in removable_elems.take(15):
    #     print(i)
    
    elems_to_remove=removable_elems.sample(0.5,1/8).distinct()


    

    # Utility Matrix ((i,j), v)
    M = cartesian_prod_w_users.map(lambda x: ((x[1][1],x[0][1][1][1]), x[0][1][0][1]))


    print("removing test elements")
    print(M.count())
    print(M.distinct().count())
    print(elems_to_remove.count())
    
    print(M.subtractByKey(elems_to_remove).count())

    print(M.take(1))
    print(elems_to_remove.collect())

    test_M = M.subtractByKey(elems_to_remove)
    
    print("consitency check")
    print(M.map(lambda x: (x[0][1], (x[0][0], x[1]))  ).groupByKey().mapValues(list).count())
    print(M.map(lambda x: (x[0][0], (x[0][1], x[1]))  ).groupByKey().mapValues(list).count())

    users_check = M.map(lambda x: (x[0][0], (x[0][1], x[1]))  ).groupByKey().mapValues(list).map(lambda x: x[0])
    users_to_add = users.map(lambda x: x[1]).subtract(users_check)

    print(users_to_add.cartesian(M).take(10))

    hashtags_test= test_M.map(lambda x: (x[0][1], (x[0][0], x[1]))  ).groupByKey().mapValues(list).count()
    users_test = test_M.map(lambda x: (x[0][0], (x[0][1], x[1]))  ).groupByKey().mapValues(list).count()
    if hashtags_test == hashtags.count() and users_test == users.count():
        print("consistency test passed")
    else:
        raise Exception
    
    return  test_M, elems_to_remove




M, test_elements = getUtilityMatrix(user_hashtags_grouped, users, hashtags, test=True)

# M, _ = getUtilityMatrix(user_hashtags_grouped)
print(M.intersection(test_elements).count())



#test

a = M.map(lambda x: x[1]).reduce(lambda x,y: x+y) / (hashtags.count() * users.count())
maxval = M.map(lambda x:x[1]).reduce(lambda x,y: x if x >= y else y)
print("value greater than 1")
print(M.filter(lambda x: x[1] > 1).take(10))
init_val = sqrt(a/2)

# init_val = 1

print("users: ", users.count())
print("hashtags: ", hashtags.count())
print("sparsity for utility matrix: ", M.count()/(users.count()*hashtags.count()))
print("\nmean for utility matrix:", a)
print("max value found in utility matrix:", maxval)
print(f"init value for U and V: {init_val}")

# Building Matrix U and V
# UxV(i,j) = somme k i,k * k,j
U = users.flatMap(lambda x: (((x[1],1), init_val), ((x[1],2), init_val)))
V = hashtags.flatMap(lambda x: (((1, x[1]), init_val), ((2, x[1]),init_val)))

UxV = Prod(U,V)
print("RMSE: ", RMSE(M,UxV))


iters=150
RMSE_history = []


for i in range(iters):
    U,V = OptimizationLoop(U,V,matrix="U")
    UxV = Prod(U,V)
    RMSE_history.append(RMSE(M,UxV))
    print("RMSE: ", RMSE_history[-1])
    U,V = OptimizationLoop(U,V,matrix="V")
    UxV = Prod(U,V)
    RMSE_history.append(RMSE(M,UxV))
    print("RMSE: ", RMSE_history[-1])



Estimated_M = UxV.cartesian(hashtags).filter(lambda x: x[0][0][1] == x[1][1])
Estimated_M_real_hashtags = Estimated_M.filter(lambda x: x[0][0][1] == x[1][1]).map(lambda x: ((x[0][0][0],x[1][0]), x[0][1]))
Estimated_M_real_users = Estimated_M_real_hashtags.cartesian(users).filter(lambda x: x[0][0][0] == x[1][1]).map(lambda x: (x[1][0],(x[0][0][1],x[0][1])))
Estimated_M_grouped = Estimated_M_real_users.groupByKey().mapValues(list)
Estimated_M_grouped.sample(0.2,1/100).map(lambda x: (x[0],sorted(x[1]))).take(100)



# test_elements = test_elements.filter(lambda x: x[1] > 0)




# for i in test_elements.take(10): print(i)

test_couples = UxV.cartesian(test_elements).filter(lambda x: x[0][0] == x[1][0])

# for i in test_couples.take(10): print(i)


error = test_couples.map(lambda x:  abs(x[0][1]-x[1][1])).reduce(lambda x,y : x+y) / test_elements.map(lambda x: x[1]).reduce(lambda x,y:x+y)

print("error: ", error)



with open('UV.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(Estimated_M_grouped.sample(0.2,1/100).map(lambda x: (x[0],sorted(x[1], key=lambda x: x[1]))).take(100))

len(RMSE_history)

