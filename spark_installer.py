import os
os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-11-openjdk"
os.environ["SPARK_HOME"] = "/home/alekeasy/uni/amd/project/spark-3.3.0-bin-hadoop3"

import findspark

findspark.init(os.environ["SPARK_HOME"])

from pyspark.sql import SparkSession
spark = SparkSession.builder.master("local[*]").getOrCreate()

if spark: print("spark succesfully started")
